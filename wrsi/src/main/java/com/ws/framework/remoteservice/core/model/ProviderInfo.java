/**
 * 
 */
package com.ws.framework.remoteservice.core.model;

/**
 * @author WSH
 *
 */
public class ProviderInfo implements Comparable<ProviderInfo> {

	/**
	 * @param schemal
	 * @param host
	 * @param port
	 */
	public ProviderInfo(String schemal, String host, int port) {
		super();
		this.schemal = schemal;
		this.host = host;
		this.port = port;
	}

	private String schemal;

	private String host;

	private int port;

	/**
	 * @return the schemal
	 */
	public String getSchemal() {
		return schemal;
	}

	/**
	 * @param schemal
	 *            the schemal to set
	 */
	public void setSchemal(String schemal) {
		this.schemal = schemal;
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host
	 *            the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port
	 *            the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	@Override
	public String toString() {
		return this.schemal + "@" + host + ":" + port;
	}

	@Override
	public int compareTo(ProviderInfo o) {
		if (null == o)
			return -1;
		else if (o.toString().equals(this.toString()))
			return 0;
		else
			return 1;
	}

}
