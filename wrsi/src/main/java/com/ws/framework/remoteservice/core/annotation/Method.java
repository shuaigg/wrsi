/**
 * 
 */
package com.ws.framework.remoteservice.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.stereotype.Component;

/**
 * @author WSH
 *
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Method {
	
	boolean idempotent() default true;
	
	int retryTimes() default 3;

	int timeout() default 3000;
	
	String description() default "";
	
}
