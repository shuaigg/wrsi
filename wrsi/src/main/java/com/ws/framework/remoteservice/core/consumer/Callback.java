/**
 * 
 */
package com.ws.framework.remoteservice.core.consumer;

/**
 * @author WSH
 *
 */
public interface Callback<T> {

	 void handlerResult(T param);

	 void handlerException(Throwable paramException);
	  
}
