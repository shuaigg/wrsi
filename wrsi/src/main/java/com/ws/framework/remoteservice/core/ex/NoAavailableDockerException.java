/**
 * 
 */
package com.ws.framework.remoteservice.core.ex;

/**
 * @author WSH
 *
 */
public class NoAavailableDockerException extends WRSIException {

	private static final long serialVersionUID = -21619926635798695L;

	public NoAavailableDockerException() {
		super();
	}

	public NoAavailableDockerException(String message) {
		super(message);
	}
	
	public NoAavailableDockerException(Throwable cause) {
		super(cause);
	}
	
    public NoAavailableDockerException(String message, Throwable cause) {
        super(message, cause);
    }
}
