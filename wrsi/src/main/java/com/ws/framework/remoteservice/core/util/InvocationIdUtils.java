/**
 * 
 */
package com.ws.framework.remoteservice.core.util;

import java.lang.management.ManagementFactory;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * @author WSH
 *
 */
public class InvocationIdUtils {

	private static String BASE = null;

    private static AtomicInteger COUNTER = new AtomicInteger();

    public static String processId;
    
    static {
        try {
            BASE = NetUtils.getLocalHost() + "-" + ManagementFactory.getRuntimeMXBean().getName();
            processId = ManagementFactory.getRuntimeMXBean().getName();
        } catch (Throwable ex) {
            int end = 100000;
            int start = 1000;
            BASE = NetUtils.getLocalHost() + "-" + String.valueOf((Math.random() * (end - start + 1)) + start);
            processId = String.valueOf((Math.random() * (end - start + 1)) + start);
        }
    }

    public static String nextId() {
        return BASE + "-" + COUNTER.incrementAndGet() + "-" + System.currentTimeMillis();
    }

}
