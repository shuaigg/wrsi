/**
 * 
 */
package com.ws.framework.remoteservice.core.consumer;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import com.ws.framework.remoteservice.core.ex.TimeoutException;

/**
 * @author WSH
 *
 */
public class SyncFuture<T> {

	private CountDownLatch latch = new CountDownLatch(1);

	private T response;

	private long beginTime = System.currentTimeMillis();

	public SyncFuture() {
	}

	public boolean isDone() {
		if (response != null) {
			return true;
		}
		return false;
	}

	public T get() throws InterruptedException {
		latch.await();
        return this.response;
	}
	
	public T get(long timeout, TimeUnit unit) throws InterruptedException {
		if (latch.await(timeout, unit)) {
			return this.response;
		} else {
			throw new TimeoutException();
		}
	}

	public void setResponse(T response) {
		this.response = response;
		latch.countDown();
	}

	public long getBeginTime() {
		return beginTime;
	}
}
