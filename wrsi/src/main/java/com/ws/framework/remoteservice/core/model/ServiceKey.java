/**
 * 
 */
package com.ws.framework.remoteservice.core.model;

import java.io.Serializable;

import org.apache.curator.shaded.com.google.common.base.Joiner;

import com.ws.framework.remoteservice.core.util.WrsiConstants;

/**
 * @author WSH
 *
 */
public class ServiceKey implements Serializable {

	private static final long serialVersionUID = 8793101240562272659L;

	/**
	 * @param contractName
	 * @param implCode
	 */
	public ServiceKey(String contractName, String implCode) {
		super();
		this.contractName = contractName;
		ImplCode = implCode;
	}

	private String contractName;

	private String ImplCode;

	/**
	 * @return the interferName
	 */
	public String getContractName() {
		return contractName;
	}

	/**
	 * @param interferName
	 *            the interferName to set
	 */
	public void setContractName(String contractName) {
		this.contractName = contractName;
	}

	/**
	 * @return the implCode
	 */
	public String getImplCode() {
		return ImplCode;
	}

	/**
	 * @param implCode
	 *            the implCode to set
	 */
	public void setImplCode(String implCode) {
		ImplCode = implCode;
	}

	@Override
	public String toString() {
		return Joiner.on(WrsiConstants.Appender).join(contractName, ImplCode);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		ServiceKey that = (ServiceKey) o;
		String ic1 = null == ImplCode ? "" : ImplCode;
		String ic2 = null ==  that.getImplCode() ? "" : that.getImplCode();
		if (contractName != null ? !contractName.equals(that.contractName) : that.contractName != null)
			return false;
		return ic1.equals(ic2);
	}

	@Override
	public int hashCode() {
		int result = contractName != null ? contractName.hashCode() : 0;
		result = 31 * result + (ImplCode != null ? ImplCode.hashCode() : 0);
		return result;
	}
}
