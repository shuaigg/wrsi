/**
 * 
 */
package com.ws.framework.remoteservice.core.ex;

/**
 * @author WSH
 *
 */
public class WRSIException extends RuntimeException {

	private static final long serialVersionUID = -5368469914992618989L;

	public WRSIException() {
		super();
	}

	public WRSIException(String message) {
		super(message);
	}
	
	public WRSIException(Throwable cause) {
		super(cause);
	}
	
    public WRSIException(String message, Throwable cause) {
        super(message, cause);
    }
}
