/**
 * 
 */
package com.ws.framework.remoteservice.core.provider;

import java.util.Set;

import com.ws.framework.remoteservice.core.model.Invocation;
import com.ws.framework.remoteservice.core.model.Result;
import com.ws.framework.remoteservice.core.model.ServiceKey;

/**
 * @author WSH
 *
 */
public interface ServiceExecutor {

	Set<ServiceKey> getRegisteredServiceKeys();
	
	void register(ServiceKey key, Object serviceImpl);
	
	Result invoker(Invocation invocation) throws Exception;
	
}
