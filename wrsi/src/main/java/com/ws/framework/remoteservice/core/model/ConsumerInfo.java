package com.ws.framework.remoteservice.core.model;

import java.io.Serializable;
import java.util.Iterator;

import com.google.common.base.Splitter;

public class ConsumerInfo implements Serializable {

    private static final long serialVersionUID = 7294417520254202029L;

    private String appCode;

    private String host;

    private String process;

    public ConsumerInfo() {
    }

    public ConsumerInfo(String process, String host, String appCode) {
        this.process = process;
        this.host = host;
        this.appCode = appCode;
    }

    public ConsumerInfo(String str, String version) {
        Iterator<String> iterator = Splitter.on("@").split(str).iterator();
        this.process = iterator.next();
        this.host = iterator.next();
        this.appCode = iterator.next();
    }

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String toString() {
        return process + "@" + host + "@" + appCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConsumerInfo that = (ConsumerInfo) o;
        if (appCode != null ? !appCode.equals(that.appCode) : that.appCode != null) return false;
        if (host != null ? !host.equals(that.host) : that.host != null) return false;
        return !(process != null ? !process.equals(that.process) : that.process != null);
    }

    @Override
    public int hashCode() {
        int result = appCode != null ? appCode.hashCode() : 0;
        result = 31 * result + (host != null ? host.hashCode() : 0);
        result = 31 * result + (process != null ? process.hashCode() : 0);
        return result;
    }
}
