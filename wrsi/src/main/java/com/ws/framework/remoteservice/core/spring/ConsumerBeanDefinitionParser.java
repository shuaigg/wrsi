/**
 * 
 */
package com.ws.framework.remoteservice.core.spring;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.parsing.BeanComponentDefinition;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.beans.factory.xml.BeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.w3c.dom.Element;

import com.ws.framework.remoteservice.core.ReferenceBeanPostProcessor;
import com.ws.framework.remoteservice.core.register.CuratorClient;

/**
 * @author WSH
 *
 */
public class ConsumerBeanDefinitionParser implements BeanDefinitionParser {

	static final String BEAN_NAME = "ws_service_servicesReference";
	
	@Override
	public BeanDefinition parse(Element element, ParserContext parserContext) {
		CuratorClient.init(element.getAttribute("address"));
		if (!parserContext.getRegistry().containsBeanDefinition(BEAN_NAME)) {
			RootBeanDefinition def = new RootBeanDefinition();
			def.setBeanClassName(ReferenceBeanPostProcessor.class.getName());
			parserContext.registerBeanComponent(new BeanComponentDefinition(def, BEAN_NAME));
		}
		return null;
	}

}
