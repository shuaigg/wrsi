/**
 * 
 */
package com.ws.framework.remoteservice.core.model;

import java.io.Serializable;

/**
 * @author WSH
 *
 */
public class Pong implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8046308860986950024L;

	public static final Pong instance = new Pong();
	
	private Pong() {}
}
