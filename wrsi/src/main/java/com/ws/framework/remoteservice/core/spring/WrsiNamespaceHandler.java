/**
 * 
 */
package com.ws.framework.remoteservice.core.spring;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

/**
 * @author WSH
 *
 */
public class WrsiNamespaceHandler extends NamespaceHandlerSupport {

	@Override
	public void init() {
		registerBeanDefinitionParser("register", new ProviderBeanDefinitionParser());  
		registerBeanDefinitionParser("consumer", new ConsumerBeanDefinitionParser());  
	}

}
