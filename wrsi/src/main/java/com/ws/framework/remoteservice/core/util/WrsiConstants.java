/**
 * 
 */
package com.ws.framework.remoteservice.core.util;

/**
 * @author WSH
 *
 */
public class WrsiConstants {
	/**
	 * zk注册协议
	 */
	public static final String Starter = "/";
	public static final String Prefix = "wrsiV1";
	public static final String Provider = "provider";
	public static final String Consumer = "consumer";
	public static final String Appender = "@";
	public static final String Separator = Starter;
	public static final String host_port_appender = ":";
	
	//锲约实现默认ImplCode
	public static final String registry_for_default_provider = "default";
	
	//默认调度超时
	public static final int DEFAULT_INVOKE_TIMEOUT_MS = 3000;
}
