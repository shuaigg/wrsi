/**
 * 
 */
package com.ws.framework.remoteservice.core.model;

/**
 * @author WSH
 *
 */
public interface Invocation {

	ServiceKey getServiceKey();

	String getMethod();

	Object[] getArguments();

	Class<?>[] getParameterTypes();

	String getId();
}
