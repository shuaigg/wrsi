/**
 * 
 */
package com.ws.framework.remoteservice.core.model;

import java.io.Serializable;

/**
 * @author WSH
 *
 */
public class InvocationImpl implements Invocation,Serializable {

	private static final long serialVersionUID = -2114297506168481715L;
	
	public InvocationImpl(ServiceKey serviceKey, String method, Object[] arguments, Class<?>[] parameterTypes, String id) {
		super();
		this.serviceKey = serviceKey;
		this.method = method;
		this.arguments = arguments;
		this.id = id;
		this.parameterTypes = parameterTypes;
	}

	private ServiceKey serviceKey;
	
	private String method;
	
	private Object[] arguments;
	
	private String id;
	
	private Class<?>[] parameterTypes;

	@Override
	public ServiceKey getServiceKey() {
		return serviceKey;
	}

	@Override
	public String getMethod() {
		return method;
	}

	@Override
	public Object[] getArguments() {
		return arguments;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public Class<?>[] getParameterTypes() {
		return parameterTypes;
	}

}
