/**
 * 
 */
package com.ws.framework.remoteservice.core.consumer;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

/**
 * @author WSH
 *
 */
public enum GenericThreadPool {

	instance;

	private final Logger logger = Logger.getLogger(GenericThreadPool.class.getName());

	private final AtomicLong atomic = new AtomicLong();

	private final int CORE_SIZE = Runtime.getRuntime().availableProcessors();
	public ThreadPoolExecutor threadPool = new ThreadPoolExecutor(CORE_SIZE, CORE_SIZE * 2, 120,
			TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(),
			new ThreadFactoryBuilder().setDaemon(true)
					.setNameFormat("Generic_invocation_thread_" + atomic.getAndIncrement()).build(),
			new RejectedExecutionHandler() {

				@Override
				public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
					logger.warning("Threadpool is overflow discard");
				}
			});

}
