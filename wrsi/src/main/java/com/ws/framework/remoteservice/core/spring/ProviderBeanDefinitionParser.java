/**
 * 
 */
package com.ws.framework.remoteservice.core.spring;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.beans.factory.xml.BeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.w3c.dom.Element;

import com.ws.framework.remoteservice.core.ServiceBean;
import com.ws.framework.remoteservice.core.register.CuratorClient;

/**
 * @author WSH
 *
 */
public class ProviderBeanDefinitionParser implements BeanDefinitionParser {
	
	private final String SERVICE_BEAN = "ws_service_bean";
	
	public ProviderBeanDefinitionParser() {
	}

	@Override
	public BeanDefinition parse(Element element, ParserContext parserContext) {
		CuratorClient.init(element.getAttribute("address"));
		if (!parserContext.getRegistry().containsBeanDefinition(SERVICE_BEAN)) {
			RootBeanDefinition beanDefinition = new RootBeanDefinition();
			beanDefinition.setBeanClassName(ServiceBean.class.getName());
			beanDefinition.setBeanClass(ServiceBean.class);
			parserContext.getRegistry().registerBeanDefinition(SERVICE_BEAN, beanDefinition);
		}
		return null;
	}

}
