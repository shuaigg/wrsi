/**
 * 
 */
package com.ws.framework.remoteservice.core.model;

import java.io.Serializable;

/**
 * @author WSH
 *
 */
public class Ping implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5375495633762728251L;
	
	public static final Ping instance = new Ping();
	
	private Ping() {}

}
