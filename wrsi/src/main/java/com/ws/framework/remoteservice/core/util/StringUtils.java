/**
 * 
 */
package com.ws.framework.remoteservice.core.util;

/**
 * @author WSH
 *
 */
public class StringUtils {

	public static boolean isEmpty(String str) {
		if (null == str || "".equals(str))
			return true;
		else
			return false;
	}
	
	public static boolean isNotEmpty(String str) {
		return !isEmpty(str);
	}
}
