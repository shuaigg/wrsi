/**
 * 
 */
package com.ws.framework.remoteservice.core.ex;

/**
 * @author WSH
 *
 */
public class ChannelInvalidException extends RuntimeException {

	private static final long serialVersionUID = 6746133424388478086L;

	public ChannelInvalidException(String msg) {
		super(msg);
	}
	
    public ChannelInvalidException(Throwable cause) {
        super(cause);
    }
}
