/**
 * 
 */
package com.ws.framework.remoteservice.core.protocal;

import java.util.List;

import com.ws.framework.remoteservice.core.model.Invocation;
import com.ws.framework.remoteservice.core.model.Ping;
import com.ws.framework.remoteservice.core.model.Pong;
import com.ws.framework.remoteservice.core.model.Result;
import com.ws.framework.remoteservice.core.provider.ServiceExecutorImpl;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;

/**
 * @author WSH
 *
 */
public class Hamal extends MessageToMessageDecoder<Object> {

	@Override
	protected void decode(ChannelHandlerContext ctx, Object object, List<Object> out) throws Exception {
		if (object instanceof Invocation) {
			Result result = ServiceExecutorImpl.instance.invoker((Invocation)object);
			ctx.channel().writeAndFlush(result);
		} if (object instanceof Ping) {
			ctx.channel().writeAndFlush(Pong.instance);
		}
	}

}
