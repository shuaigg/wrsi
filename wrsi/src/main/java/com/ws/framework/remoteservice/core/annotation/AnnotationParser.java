/**
 * 
 */
package com.ws.framework.remoteservice.core.annotation;

import java.lang.annotation.Annotation;


/**
 * @author WSH
 *
 */
public final class AnnotationParser {

    public static Implement parseImplement(Class<?> clz) {
        Annotation[] annotations = clz.getDeclaredAnnotations();
        if (annotations == null || annotations.length == 0) {
            return null;
        }
        for (Annotation annotation : annotations) {
            if (annotation instanceof Implement) {
                return (Implement) annotation;
            }
        }
        return null;
    }
    
}
