/**
 * 
 */
package com.ws.framework.remoteservice.core.model;

import java.io.Serializable;

/**
 * @author WSH
 *
 */
public class Result implements Serializable {

	private static final long serialVersionUID = 1812117980872866306L;
	
	/**
	 * @param index
	 * @param value
	 */
	public Result(String id, Object value) {
		super();
		this.id = id;
		this.value = value;
	}
	
	public Result(String id) {
		super();
		this.id = id;
	}

	private String id;
	
	private Object value;
	
	private boolean success;
	
	private Throwable throwable;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param index the id to set
	 */
	public Result setId(String id) {
		this.id = id;
		return this;
	}

	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public Result setValue(Object value) {
		this.value = value;
		return this;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success the success to set
	 */
	public Result setSuccess(boolean success) {
		this.success = success;
		return this;
	}

	/**
	 * @return the throwable
	 */
	public Throwable getThrowable() {
		return throwable;
	}

	/**
	 * @param throwable the throwable to set
	 */
	public Result setThrowable(Throwable throwable) {
		this.throwable = throwable;
		return this;
	}
}
