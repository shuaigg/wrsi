/**
 * 
 */
package com.ws.framework.remoteservice.core.consumer;

/**
 * @author WSH
 *
 */
public interface ProxyFactory {

	Object getProxy(ServiceAgent agent);
	
}
