/**
 * 
 */
package com.ws.framework.remoteservice.core.config;

/**
 * @author WSH
 *
 */
public class RegistryConfig {

	private String address;
	
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	
}
