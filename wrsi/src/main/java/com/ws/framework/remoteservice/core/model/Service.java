/**
 * 
 */
package com.ws.framework.remoteservice.core.model;

/**
 * @author WSH
 *
 */
public interface Service {

	/**
     * 服务唯一标识
     *
     * @return 唯一标识
     */
    String getId();

    /**
     * 服务契约
     *
     * @return 服务契约接口类
     */
    Class<?> getContract();

    /**
     * 服务契约
     *
     * @return 服务契约接口类的字符串
     */
    String getContractStr();

    /**
     * 契约的实现编号，一个契约可能同时存在多种实现
     *
     * @return 实现编号
     */
    String getImplCode();

    
}
