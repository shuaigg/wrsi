/**
 * 
 */
package com.ws.framework.remoteservice.core.ex;

/**
 * @author WSH
 *
 */
public class AnnotationNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -5368469914992618989L;

	public AnnotationNotFoundException() {
		super();
	}

	public AnnotationNotFoundException(String message) {
		super(message);
	}
}
