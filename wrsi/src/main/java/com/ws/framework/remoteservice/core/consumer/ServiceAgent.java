/**
 * 
 */
package com.ws.framework.remoteservice.core.consumer;

import java.lang.reflect.Method;

import com.ws.framework.remoteservice.core.model.Result;

/**
 * @author WSH
 *
 */
public interface ServiceAgent {

    String getContract();

    String getImplCode();
    
    /**
     * 同步调用(默认超时 3000毫秒)
     * @param method
     * @param args
     * @return
     */
    Object invoke(Method method, Object[] args);
    
    /**
     * 异步调用 返回 SyncFuture(默认超时 3000毫秒)
     * @param method
     * @param args
     * @return
     */
    SyncFuture<Result> invokeWithFuture(Method method, Object[] args);
    
    /**
     * 支持 callback handler异步处理(注意:只有成功了才执行)(默认超时3000毫秒)
     * @param method
     * @param args
     * @param runnable
     */
    void invokeWithCallback(Method method, Object[] args, Callback<Object> callback);
}
