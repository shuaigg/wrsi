/**
 * 
 */
package com.ws.framework.remoteservice.core.consumer;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.ws.framework.remoteservice.core.ex.NoAavailableDockerException;
import com.ws.framework.remoteservice.core.model.ServiceKey;
import com.ws.framework.remoteservice.core.protocal.Docker;
import com.ws.framework.remoteservice.core.protocal.DockerManager;

/**
 * @author WSH
 *
 */
public enum LoadBalance {
	
	Random {
		public Docker select(List<Docker> exclusive, ServiceKey serviceKey) {
			List<Docker> list = DockerManager.instance.getDocker(serviceKey);
			list.removeAll(exclusive);
			if (list.isEmpty()) 
				throw new NoAavailableDockerException();
			Collections.shuffle(list);
			return list.get(0);
		}
	},
	Polling {
		public Docker select(List<Docker> exclusive, ServiceKey serviceKey) {
			List<Docker> list = DockerManager.instance.getDocker(serviceKey);
			list.removeAll(exclusive);
			if (list.isEmpty()) 
				throw new NoAavailableDockerException();
			Collections.sort(list, new Comparator<Docker>() {
				@Override
				public int compare(Docker o1, Docker o2) {
					return o1.getInfo().hashCode() - o2.getInfo().hashCode();
				}
			});
			return list.get(0);
		}
	};
	
	public abstract Docker select(List<Docker> exclusive, ServiceKey serviceKey);
}
