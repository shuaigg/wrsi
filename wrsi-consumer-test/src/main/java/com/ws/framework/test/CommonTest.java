/**
 * 
 */
package com.ws.framework.test;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.ws.framework.remoteservice.core.consumer.Callback;
import com.ws.framework.remoteservice.core.consumer.ServiceAgent;
import com.ws.framework.remoteservice.core.consumer.ServiceLocator;
import com.ws.framework.remoteservice.core.consumer.SyncFuture;
import com.ws.framework.remoteservice.core.model.Result;
import com.ws.framework.service.MyService;

/**
 * @author WSH
 *
 */
public class CommonTest {

	@SuppressWarnings({ "unused", "resource" })
	public static void main(String[] args) throws Exception {
		 ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("spring-consumer.xml");
			 new CommonTest().run();
	}
	
	MyService service = ServiceLocator.getService(MyService.class,"myServiceImpl");
	ServiceAgent agent = ServiceLocator.getServiceAgent(MyService.class, "myServiceImpl");
	public void run() throws Exception {
		//同步调用测试
		System.out.println(service.getName());
		
		//异步future步调用测试
		SyncFuture<Result> future = agent.invokeWithFuture(MyService.class.getMethod("getNameById", new Class[]{long.class}), new Object[]{100});
		System.out.println(future.get().getValue());
		
		//异步callback调用测试
		agent.invokeWithCallback(MyService.class.getMethod("getNameById", new Class[]{long.class}), new Object[]{"200"}, new Callback<Object>() {

			@Override
			public void handlerResult(Object param) {
				System.out.println(param);
			}

			@Override
			public void handlerException(Throwable paramException) {
				System.out.println("handler errors: "  + paramException);
			}
		});
	}
}
