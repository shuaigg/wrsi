/**
 * 
 */
package com.ws.framework.test;

import com.ws.framework.remoteservice.core.annotation.Reference;
import com.ws.framework.service.MyService;

/**
 * @author WSH
 *
 */
public class ConsumerBuzLogic {

	@Reference(contract=MyService.class, implCode="myServiceImpl")
	public MyService service;
	
	public void doSomething() {
		System.out.println(service.getName());
		System.out.println(service.getNameById(3000L));
	}
	
}
