/**
 * 
 */
package com.ws.framework.test;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author WSH
 *
 */
public class AutowiredTest {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("spring-consumer.xml");
		ConsumerBuzLogic logic = classPathXmlApplicationContext.getBean(ConsumerBuzLogic.class);
		logic.doSomething();
	}
}
