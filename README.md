# RPC 框架 wrsi - 使用说明

当前版本：0.0.1-SNAPSHOT

发布日期：20170525

发布日志参见 `RELEASE.md` 文档


## 发布 RPC 服务

> 参见 wrsi-provider-test 模块

### 第一步：添加 Maven 依赖

#### pom.xml

```xml
<dependency>
    <groupId>com.ws.framework.rpc</groupId>
    <artifactId>wrsi</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>

<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-context</artifactId>
    <version>${version.rpc}</version>
</dependency>
```

### 第二步：定义接口
#### com.ws.framework.service.MyService

```java
package com.ws.framework.service;

/**
 * @author WSH
 *
 */
public interface MyService {

	public String getName();
	
	public String getNameById(long id);
	
}
```

### 第三步：实现接口，打上注解@Implement准备暴露服务
#### com.ws.framework.service.MyService

```java
package com.ws.framework.service.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.ws.framework.remoteservice.core.annotation.Implement;
import com.ws.framework.service.MyService;

/**
 * @author WSH
 * 
 */
@Implement(contract = MyService.class, implCode="myServiceImpl")
public class MyServiceImpl implements MyService {

	private List<String> names = Arrays.asList("铁血史泰龙", "花姑娘鲁智深", "一点梅", "", "皇家巴萨");
	
	public String getName() {
		return "二爷关羽在此";
	}

	public  String getNameById(long id) {
		synchronized (names) {
			Collections.shuffle(names);
		}
		return id + "-" + names.get(0);
	}

}
```
- @Implement注解中 有多个实现类要制定implCode,只有一个可以不填

### 第四步：配置 rpc-provider 服务端(填写注册中心地址)

#### spring.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xmlns:ws="http://www.wsframework.com/schema/remote/info"
	xsi:schemaLocation="    
     http://www.springframework.org/schema/beans 
     http://www.springframework.org/schema/beans/spring-beans.xsd
     http://www.wsframework.com/schema/remote/info 
     http://www.wsframework.com/schema/remote/info/registry.xsd ">

	<bean id="testService" class="com.ws.framework.service.impl.MyServiceImpl"></bean>
	
    <ws:register address="127.0.0.1:2181" />
</beans>
```

- ws:register：用于服务注册，需提供 ZooKeeper 地址

注册到 ZooKeeper 中的 ZNode 路径为：`wrsiV1/com.ws.framework.service.MyService@myServiceImpl/provider/address`，前 3 个节点是持久的，最后 1 个节点是临时的

### 第五步：启动 rpc-provider 服务

```java
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author WSH
 *
 */
public class Start {

	@SuppressWarnings({ "unused", "resource" })
	public static void main(String[] args) {
		 ApplicationContext ac = new ClassPathXmlApplicationContext("spring-provider.xml");  
	}
}
```

运行 Start 类，将对外发布 RPC 服务，同时进行服务注册



## 调用 RPC 服务

> 参见 wrsi-consumer-test 模块

### 第一步：添加 Maven 依赖

```xml
<!-- rpc -->
<dependency>
    <groupId>com.ws.framework.rpc</groupId>
    <artifactId>wrsi</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>

<!-- spring -->
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-context</artifactId>
    <version>${spring.version}</version>
</dependency>

<!-- provider提供的接口层jar-->
<dependency>
    <groupId>com.ws.framework.rpc</groupId>
    <artifactId>interfaces</artifactId>
    <version>0.0.1</version>
    <scope>system</scope>
    <systemPath>${project.basedir}/src/main/resources/service.jar</systemPath>
</dependency>
```

### 第二步：编写自己业务类 准备使用rpc服务(注解方式)

```java
import com.ws.framework.remoteservice.core.annotation.Reference;
import com.ws.framework.service.MyService;

/**
 * @author WSH
 *
 */
public class ConsumerBuzLogic {

	@Reference(contract=MyService.class, implCode="myServiceImpl")
	public MyService service;
	
	public void doSomething() {
		System.out.println(service.getName());
		System.out.println(service.getNameById(3000L));
	}
	
}
```

- Reference注解使用时请注意 确保implCode的正确性

### 第三步：配置 rpc-consumer 客户端，订阅注册中心

#### spring.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xmlns:ws="http://www.wsframework.com/schema/remote/info"
	xsi:schemaLocation="    
     http://www.springframework.org/schema/beans 
     http://www.springframework.org/schema/beans/spring-beans.xsd
     http://www.wsframework.com/schema/remote/info 
     http://www.wsframework.com/schema/remote/info/registry.xsd ">

    <ws:consumer address="127.0.0.1:2181" />
    
    <bean id="logic" class="com.ws.framework.test.ConsumerBuzLogic"></bean>
</beans> 
```

- ws:consumer：ZooKeeper 服务器的地址（IP 地址与端口）


### 第四步：调用 RPC 服务

```java
package com.ws.framework.test;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author WSH
 *
 */
public class AutowiredTest {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("spring-consumer.xml");
		ConsumerBuzLogic logic = classPathXmlApplicationContext.getBean(ConsumerBuzLogic.class);
		logic.doSomething();
	}
}
```

### ps:  支持非注解时调用。支持（同步，Future, Callback)

```java
package com.ws.framework.test;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.ws.framework.remoteservice.core.consumer.Callback;
import com.ws.framework.remoteservice.core.consumer.ServiceAgent;
import com.ws.framework.remoteservice.core.consumer.ServiceLocator;
import com.ws.framework.remoteservice.core.consumer.SyncFuture;
import com.ws.framework.remoteservice.core.model.Result;
import com.ws.framework.service.MyService;

/**
 * @author WSH
 *
 */
public class CommonTest {

	@SuppressWarnings({ "unused", "resource" })
	public static void main(String[] args) throws Exception {
		 ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("spring-consumer.xml");
			 new CommonTest().run();
	}
	
	MyService service = ServiceLocator.getService(MyService.class,"myServiceImpl");
	ServiceAgent agent = ServiceLocator.getServiceAgent(MyService.class, "myServiceImpl");
	public void run() throws Exception {
		//同步调用测试
		System.out.println(service.getName());
		
		//异步future步调用测试
		SyncFuture<Result> future = agent.invokeWithFuture(MyService.class.getMethod("getNameById", new Class[]{long.class}), new Object[]{100});
		System.out.println(future.get().getValue());
		
		//异步callback调用测试
		agent.invokeWithCallback(MyService.class.getMethod("getNameById", new Class[]{long.class}), new Object[]{"200"}, new Callback<Object>() {

			@Override
			public void handlerResult(Object param) {
				System.out.println(param);
			}

			@Override
			public void handlerException(Throwable paramException) {
				
			}
		});
	}
}
```