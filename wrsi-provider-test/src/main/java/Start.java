import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 
 */

/**
 * @author WSH
 *
 */
public class Start {

	@SuppressWarnings({ "unused", "resource" })
	public static void main(String[] args) {
		 ApplicationContext ac = new ClassPathXmlApplicationContext("spring-provider.xml");  
	}
}
