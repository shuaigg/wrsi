/**
 * 
 */
package com.ws.framework.service.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.ws.framework.remoteservice.core.annotation.Implement;
import com.ws.framework.service.MyService;

/**
 * @author WSH
 *
 */
@Implement(contract = MyService.class, implCode="myServiceImpl")
public class MyServiceImpl implements MyService {

	private List<String> names = Arrays.asList("铁血史泰龙", "花姑娘鲁智深", "一点梅", "", "皇家巴萨");
	
	public String getName() {
		return "二爷关羽在此";
	}

	public  String getNameById(long id) {
		synchronized (names) {
			Collections.shuffle(names);
		}
		return id + "-" + names.get(0);
	}

}
