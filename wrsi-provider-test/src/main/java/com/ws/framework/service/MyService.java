/**
 * 
 */
package com.ws.framework.service;

/**
 * @author WSH
 *
 */
public interface MyService {

	public String getName();
	
	public String getNameById(long id);
	
}
